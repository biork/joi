
/**
	* Perform a join on the lines of two files, a and b based on the
	* value of a fixed-digit-count hexadecimal number in the first column.
	*
	* This program is intended to identify SNV positions (or other single-
	* basepair features) common to two genetics files AFTER the files' SNV
	* positions have been transformed from 'contig position' format (e.g.
	* "chr9 123456") to the 9-digit [h]exadecimal-[c]oded [p]osition format.
	*
	* Implementation notes:
	* MATCH_FORMAT relies on relatively obscure capabilities of printf.
	*
	* Tested with following.
	* #include <stdio.h>
	* #include <stdlib.h>
	* int main( int argc, char *argv[] ) {
	*		printf( "%.*s%s\n", atoi(argv[1]), argv[2], argv[3] );
	*		return 0;
	*	}
	*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>
#include <ctype.h>
#include <assert.h>

#include "lines.h"

#define HEX_CODED_POS_LENGTH   (9)
#define MAX_COUNT_HEADER_LINES (10)

static int arg_key_width = 0; // to disable checking
static int arg_base      = 10;
static bool arg_emit_all = false;
static const char *arg_match_prefix = "";
static int  arg_action   = 'r';   // ...action to take on unencodable lines.
static const char *ACTIONS = "pdr";
static const char *arg_stats_prefix = NULL;

/**
 * AcCOUNT for _every_ line!
 * */
enum COUNT_TYPES {
	C_LUNIQ, // left source has unique line (no match in right)
	C_MATCH, // left and right lines match
	C_RUNIQ, // right source has unique line (no match in left)
	C_LSKIP, // counts a "skipped" left line (prefixed with '\r\n#')
	C_RSKIP, // counts a "skipped" right line (prefixed with '\r\n#')
	// Total lines read from a source is maintained in struct _line_stream.
	C_COUNT
};

static int counts[C_COUNT] = {0,0,0,0,0};

static void _panic( unsigned int line ) {
	fprintf( stderr, "panic at %s:%d", __FILE__, line );
	fflush( stderr );
	exit(-1);
}


/**
 * EVERY line that does not begin with a newline, carriage return or hash
 * char is ASSUMED to be a key line.
 * */
static bool maybe_keyline( const char *line ) {
	return strchr( "#\n\r", line[0] ) == NULL;
}


static void _handle_non_keyline( const char *line, FILE *fp ) {

	switch( arg_action ) {
	case 'p':
		fputs( line, fp );
		break;
	case 'r':
		fputs( line, stderr );
		break;
	case 'd':
		;// nothing to do
	}
}


static const char *LINE_ENDS = "\n\r";

static const char *HEX_DIGITS
	= "0123456789abcdefABCDEF";

static const char *MATCH_FORMAT = "%s%.*s%s";

static const char *BAD_LINE_MSG
	= "error: unexpected line prefix at %s:%d: %s";
static const char *ERR_MSG_MONOTONIC
	= "error: non-increasing prefix at %s:%d: %s";

typedef struct _keyed_lines {

	line_stream input;

	/**
		* Position needs to be signed because:
		* 1. 0 is valid key in anticipated input (e.g. BED files)
		* 2. I want to initialize it to a value which won't trigger non-
		*    monotonicity on first read AND doesn't require a special
		*    conditional.
		* So initial value must be < 0.
		* More importantly, in the motivating application key encodes
		* chromosome ordinal AND basepair position, so 32-bit is insufficient.
		*/

	int64_t key;
	bool tallied; // to preclude double counting and emission

} keyed_lines;

/**
	* Scan the trailing source until it catches up with the head (if the head
	* is non-NULL). Otherwise, scan until we get a valid line in trailing.
	* Returns:
	*		 0 on normal exit (pass OR end-of-file)
	*   -1 on error
	*/
static int _catch_up( keyed_lines *t/*trailing*/, keyed_lines *h /*head or NULL*/, bool trailer_is_left, FILE *fp ) {

	int error = 0;
	char *endptr = NULL;

	while( lines_next( &t->input ) ) {

		if( ! maybe_keyline( t->input.line ) ) {
			counts[ trailer_is_left ? C_LSKIP : C_RSKIP ] += 1;
			_handle_non_keyline( t->input.line, fp );
			continue;
		}

		// Assume first field is numeric, but verify after parse.

		const long n = strtol( t->input.line, &endptr, arg_base );
		
		t->tallied = false;

		// We expect most lines except possibly a few initial and final lines
		// (headers and footers) to be valid.

		if( ! isspace( *endptr ) || (arg_key_width > 0 && (endptr - t->input.line) != arg_key_width ) ) {

			fprintf( stderr, BAD_LINE_MSG, t->input.name, t->input.count, t->input.line );
			error = -1;
			break;

		} else { // we probably have a well-formed line

			// Confirm monotonic INCREASING, not merely non-decreasing.

			if( n <= t->key ) {
				fprintf( stderr, ERR_MSG_MONOTONIC, t->input.name, t->input.count, t->input.line );
				error = -1;
				break;
			} else {
				t->key = n;
			}

			// Have we arrived at goal? ...if there is a goal. At initialization
			// as well as after the trailer catches up to the head, the goal is
			// simply one (more) valid line.

			if( h == NULL ) {
				break; // because we have a valid line
			} else
			if( t->key >= h->key ) {

				if( t->key > h->key ) {

					// Trailer has passed head. Trailer may yet match something when
					// roles are reversed, so we hold it (no emit and no tally).
					// The head MIGHT have already been emitted and tallied as a match
					// to the PREVIOUS trailing record. IF AND ONLY IF HEAD HAS NOT
					// BEEN EMITTED AND TALLIED, emit and tally it now.

					if( ! h->tallied ) {
						counts[ trailer_is_left ? C_RUNIQ : C_LUNIQ ] += 1;
						h->tallied = true;
						if( arg_emit_all ) {
							fprintf( fp, "%c\t%s", trailer_is_left ? '>' : '<', h->input.line );
						}
					}
					break; // because trailer has passed the head

				} else { // lines' keys match!

					counts[ C_MATCH ] += 1;

					// Report matches in consistent L,R order eliding the key from R,
					// accounting for the fact that the key may be the only column in
					// R! If R is multi-column:
					// 	1. Print L without its terminating NL, and
					// 	2. print the tail of R starting at the tab separator.
					// else:
					// 	1. Print L verbatim.

					if( trailer_is_left ) {

						const size_t SKIP = strspn( h->input.line, HEX_DIGITS ); // TODO: This should depend on base!
						const char *tail_of_r = h->input.line + SKIP;

						assert( t->input.line[ t->input.llen-1 ] == '\n' );

						// TODO: Following length prefixing won't work for combination CRNL line endings.
						if( ! strchr( LINE_ENDS, *tail_of_r ) /* R is multicolumn */ ) {
							fprintf( fp, MATCH_FORMAT, arg_match_prefix, (int)(t->input.llen-1), t->input.line, tail_of_r );
						} else { /* R is only one column, the key! */
							assert( strchr( LINE_ENDS, *tail_of_r ) );
							fprintf( fp, "%s%s", arg_match_prefix, t->input.line ); // only need to emit L
						}

					} else { // trailer is the right source

						assert( h->input.line[ h->input.llen-1 ] == '\n' );

						// TODO: Following length prefixing won't work for combination CRNL line endings.
						if( ! strchr( LINE_ENDS, *endptr ) /* L is multicolumn*/  ) {
							fprintf( fp, MATCH_FORMAT, arg_match_prefix, (int)(h->input.llen-1), h->input.line, endptr );
						} else { /* L is only one column, the key! */
							assert( strchr( LINE_ENDS, *endptr ) );
							fprintf( fp, "%s%s", arg_match_prefix, h->input.line ); // only need to emit R
						}
					}
					h->tallied = true; // BTW, if not for this, head arg could be const.
					t->tallied = true;
				} // match

				// Notice we don't exit _catch_up here; let tail move PAST head.

			} /* trailer >= head */ else { assert( t->key < h->key );

				counts[ trailer_is_left ? C_LUNIQ : C_RUNIQ ] += 1;
				t->tallied = true;

				if( arg_emit_all ) {
					// Trailer has not yet reached head, so emit it prefixed
					// if exhaustive output was requested.
					fprintf( fp, "%c\t%s", trailer_is_left ? '<' : '>', t->input.line );
				}

			} // trailer < head
		} // end of processing well-formatted line
	}
	return error;
}


static const char *USAGE =
"%s [ options ] <L filename> <R filename>\n"
"    --base | -b   base of integer keys [%d]\n"
"   --width | -w   expected FIXED width of key field [%d]\n"
"                  0 disable validation of field width.\n"
"  --genome | -G   implies \"-b 16 -w 9\", appropriate for H. sapiens SNV positions.\n"
"OUTPUT OPTIONS\n"
"     --all | -a   emit ALL lines, not just matching [%s]\n"
"  --prefix | -p   prefix matches, not just mismatches, implies \"--all\"\n"
"  --action | -A <{p,d,r}> Action for unencodable lines (e.g. headers).['%c']\n"
"                 [p]reserve: Emit the line verbatin in stdout.\n"
"                 [d]iscard: Discard the line (quietly).\n"
"                 [r]eport: Discard the line but report it on stderr.\n"
"   --stats | -s  A string to prefix count statistics in output tail.\n"
"                 Otherwise, counts are not reported.\n"
"   --quiet | -q  Don't emit the join. Stats are still output if requested,\n"
"                 but --quiet without --stats emits nothing!\n"
"    --help | -?   show this message\n"
"\n"
"Notes:\n"
"1. Keys must be in leftmost columns (with _no_ preceding whitespace).\n"
"2. Keys must be monotonically _increasing_, not merely non-decreasing.\n"
"3. Statistics, if requested, are always emitted on stdout.\n"
"4. By default, only matches are emitted and without prefixes.\n"
#ifdef _DEBUG
"This is a debug build.\n"
#endif
"\n";


static void _print_usage( const char *exename, FILE *fp ) {
	fprintf( fp, USAGE, exename, 
			arg_base, 
			arg_key_width, 
			arg_emit_all ? "yes" : "no",
			arg_action );
}


int main( int argc, char *argv[] ) {

	int exit_status = EXIT_SUCCESS;
	const char *arg;
	FILE *join_output_fp = stdout;
	bool scan_error = false;

	keyed_lines lin, rin;
	lin.key = INT64_MIN;
	rin.key = INT64_MIN;

	memset( counts, 0, sizeof(counts) );

	if( argc < 3 ) {
		_print_usage( argv[0], stdout );
		exit(-1);
	}

	do {

		static const char *CHAR_OPTIONS = "b:w:GpaA:s:q?";
		static struct option LONG_OPTIONS[] = {

			{"base",    1,0,'b'},
			{"width",   1,0,'w'},
			{"genome",  0,0,'G'},
			{"prefix",  0,0,'p'},
			{"all",     0,0,'a'},
			{"action",  1,0,'A'},
			{"stats",   1,0,'s'},
			{"quiet",   0,0,'q'},
			{"help",    0,0,'?'},
			{ NULL,     0,0, 0 }
		};
		int opt_index;
		const int c = getopt_long( argc, argv, CHAR_OPTIONS, LONG_OPTIONS, &opt_index );
		if( -1 == c ) break;
		switch( c ) {

		case 'b':
			arg_base = atoi(optarg);
			if( arg_base > 16 ) // sanity check
				_panic(__LINE__);
			break;

		case 'w':
			arg_key_width = atoi(optarg);
			if( arg_key_width > 16 ) // sanity check
				_panic(__LINE__);
			break;

		case 'G':
			arg_key_width = 9;
			arg_base = 16;
			break;

		case 'p':
			arg_match_prefix = "=\t";
			// fall through...
		case 'a':
			arg_emit_all = true;
			break;


		case 'A':
			arg_action = tolower(optarg[0]);
			if( strchr( ACTIONS, arg_action ) == NULL ) {
				goto usage;
			}
			break;

		case 's':
			arg_stats_prefix = optarg;
			break;

		case 'q':
			join_output_fp = fopen("/dev/null","w"); // for debugging
			break;

usage:
		case '?':
			_print_usage( argv[0], stdout );
			exit(0);
		default:
			break;
		}
	} while(true);

	arg = argv[ optind++ ];
	if( lines_init( &lin.input, arg, NULL ) != 0 ) {
		fprintf( stderr, "error: %s: opening %s\n", strerror(errno), arg );
		exit(-1);
	}

	if( optind < argc ) {
		arg = argv[ optind++ ];
		if( lines_init( &rin.input, arg, NULL ) ) {
			fprintf( stderr, "error: %s: opening %s\n", strerror(errno), arg );
			exit(-1);
		}
	} else {
		lines_init( &rin.input, "stdin", stdin ); // can't fail.
	}

	/**
		* Initialize the ping-pong...
		* Prime the left source with the first valid line, and then catch
		* the right source up to that.
		*/

	if( _catch_up( &lin, NULL, true,  join_output_fp ) != 0 || lin.input.llen < 0 ) {
		goto no_matches; // error, or left source is empty!
	}
	if( _catch_up( &rin, &lin, false, join_output_fp ) != 0 || rin.input.llen < 0 ) {
		goto no_matches; // error, or right source could not catch up to left.
	}

	assert( lin.key < rin.key );

	while( true ) {

		if( lin.key < rin.key ) {
			scan_error = _catch_up( &lin, &rin, true,  join_output_fp ) < 0;
			if( scan_error || lin.input.llen < 0 )
				break;
		} else {
			scan_error = _catch_up( &rin, &lin, false, join_output_fp ) < 0;
			if( scan_error || rin.input.llen < 0 )
				break;
		}
	}

	// Either an error was encountered, or one file should be exhausted.

	if( ! ( scan_error || lin.input.llen < 0 || rin.input.llen < 0 ) ) {
		_panic( __LINE__ );
	}

no_matches:

	if( scan_error ) {

		exit_status = EXIT_FAILURE; // ...and nothing below matters.

	} else {

		// ONLY ONE FILE SHOULD CONTAIN UNREAD ITEMS at this point.
		//
		// Matches are reported immediately upon discovery in _catch_up, so
		// there are never accumulated matches to report after loop exit as
		// in some algorithms, but we need to exhaust both files to complete
		// statistics (and arg_emit_all requires items remaining in the
		// unexhausted file to be emitted as non-matches). 
		//
		// Following is essentially an abbrevation of code in _catch_up to deal
		// with the trailing source, only relevant if arg_emit_all.

		const bool L_HAS_REMNANTS = rin.input.llen < 0;
		const char TAG = L_HAS_REMNANTS ?     '<' : '>';
		const int UNIQ = L_HAS_REMNANTS ? C_LUNIQ : C_RUNIQ;
		keyed_lines * const rem = L_HAS_REMNANTS ? &lin  : &rin;

		// The record that triggered loop exit may have been held back from
		// emission.

		if( ! rem->tallied /* then it wasn't emitted either */ ) {
			assert( maybe_keyline( rem->input.line ) );
			if( arg_emit_all ) {
				fprintf( join_output_fp, "%c\t%s", TAG, rem->input.line );
			}
			counts[UNIQ] += 1;
		}

		// TODO: tail is not parsed to verify key lines really ARE keylines.
		//
		while( lines_next( &rem->input ) ) {
			if( maybe_keyline( rem->input.line ) ) {
				if( arg_emit_all ) {
					fprintf( join_output_fp, "%c\t%s", TAG, rem->input.line );
				}
				counts[UNIQ] += 1;
			} else {
				counts[ L_HAS_REMNANTS ? C_LSKIP : C_RSKIP ] += 1;
				_handle_non_keyline( rem->input.line, join_output_fp );
			}
		}

		if( arg_stats_prefix ) {
			fprintf( stdout, "%s%d\t%d\t%d\n",
				arg_stats_prefix, counts[C_LUNIQ], counts[C_MATCH], counts[C_RUNIQ] );
		}

		// Verify counts are reconcilable.
		const char *extra = getenv("JOI_ENVIRON") ? getenv("JOI_ENVIRON") : "N/A";

		if(lin.input.count-counts[C_LSKIP] != counts[C_LUNIQ]+counts[C_MATCH]) {
			fprintf( stderr,
				"discordant left counts:%d total - %d skipped != %d left-unique + %d matching (%s)\n",
				lin.input.count,
				counts[C_LSKIP],
				counts[C_LUNIQ],
				counts[C_MATCH],
				extra );
			exit_status = EXIT_FAILURE;
		}
		if(rin.input.count-counts[C_RSKIP] != counts[C_RUNIQ]+counts[C_MATCH]) {
			fprintf( stderr,
				"discordant right counts:%d total - %d skipped != %d right-unique + %d matching (%s)\n",
				rin.input.count,
				counts[C_RSKIP],
				counts[C_RUNIQ],
				counts[C_MATCH],
				extra );
			exit_status = EXIT_FAILURE;
		}
	}

	lines_free( &rin.input );
	lines_free( &lin.input );

	return exit_status;
}

