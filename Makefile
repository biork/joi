
include common.mk

EXECUTABLE=joi

#ifdef DEV
#CPPFLAGS+=-DHAVE_SIMPLE_SUM_FOR_TESTING
#DEBUG=true
#endif

all : $(EXECUTABLE)

$(EXECUTABLE) : main.o lines.o
	$(CC) -o $@ $^

install : $(EXECUTABLE)
	install $(EXECUTABLE) $(INSTALLDIR)

uninstall :
	rm $(INSTALLDIR)/$(EXECUTABLE)

clean :
	rm -rf $(EXECUTABLE) *.o

.PHONY : clean install uninstall

