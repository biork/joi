# vim: ts=2:sw=2:sts=0:noet:nu

"""
Output of joi (with the --all option) applied to two tables will not in
general be a table--rows with be "ragged."

This utility inserts placeholders based on joi's first output column.
This indicates whether a record is present only in the left ('<'), in
both ('='), or only in the right ('>') file.

For example...

 Left file
----------
100 A B C
123 X Y Z

Right file
----------
123 D	E
200 F G

Output BEFORE tabularize.py
---------------------------
< 100 A B C
= 123 X Y Z D E
> 200 F G

Output AFTER tabularize.py
--------------------------
100 A B C . .
123 X Y Z D E
200 . . . F G
"""

from sys import argv,stdin,exit

if len(argv) < 3:
	print( argv[0], "<left columns>", "<right columns> [ <NA indicator> ]" )
	exit(0)

_COLSEP = '\t'

_L_DATA_COLS = int(argv[1])-1
_R_DATA_COLS = int(argv[2])-1
_NA = argv[3] if len(argv) > 3 else "."

_L_PLACEHOLDER = _COLSEP.join( [_NA,]*_L_DATA_COLS )
_R_PLACEHOLDER = _COLSEP.join( [_NA,]*_R_DATA_COLS )

for line in stdin:

	if line.startswith('#'):
		continue
	else:	
		if line[0] not in "<=>":
			raise RuntimeError("every line should begin with one of: <,=,>,#")

	# We omit the indicator character from output, so stip it off now to
	# simplify column count checks below.

	f = line.rstrip().split(_COLSEP)
	t = f.pop(0)

	if   '<' == t:
		assert len(f) == _L_DATA_COLS + 1
		print( *f, _R_PLACEHOLDER,           sep=_COLSEP )
	elif '=' == t:
		assert len(f) == _L_DATA_COLS + _R_DATA_COLS + 1
		print( *f,                           sep=_COLSEP )
	else:
		assert '>' == t
		assert len(f) ==                _R_DATA_COLS + 1
		print( f[0], _L_PLACEHOLDER, *f[1:], sep=_COLSEP )

