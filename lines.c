
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "lines.h"

int lines_init( line_stream *s, const char *filename, FILE *fp ) {

	int err = 0;
	memset( s, 0, sizeof(line_stream) );

	if( fp ) {

		s->name = filename ? filename : "?";
		s->fp = fp;

	} else {

		if( filename ) {
			s->fp = fopen( filename, "r" );
			if( s->fp == NULL )
				err = -1;
			else {
				s->name = strdup(filename);
				if( s->name == NULL ) {
					fclose( s->fp );
					err = -1;
				} else
					s->file_data_owned = true;
			}
		}
	}
	return err;
}


bool lines_next( line_stream *s ) {
	s->llen = getline( &s->line, &s->blen, s->fp );
	if( s->llen > 0 ) {
		s->count += 1;
		return true;
	} else
		return false;
}


void lines_free( line_stream *s ) {
	if( s->file_data_owned ) {
		if( s->fp )
			fclose( s->fp );
		if( s->name )
			free( (void*)s->name );
	}
	if( s->line )
		free( s->line );
}

