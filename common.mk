
INSTALLDIR=$(HOME)/bin

HTSINC=$(HOME)/bin/hts/include
HTSLIB=$(HOME)/bin/hts/lib/libhts.a

CFLAGS=-Wall -std=c99
CPPFLAGS=-I./lib -I$(HTSINC) \
	-D_FILE_OFFSET_BITS=64 \
	-D_DEFAULT_SOURCE \
	-D_POSIX_C_SOURCE=200809L

# Needed _DEFAULT_SOURCE to get MAP_ constants for mmap.
# Needed _POSIX_C_SOURCE=200809L to insure strndup availability which
# 	obviates making copies of const strings to writable buffers.

ifdef DEBUG
CFLAGS+=-g
CPPFLAGS+=-D_DEBUG
else
CFLAGS+=-O3
CPPFLAGS+=-DNDEBUG
endif

