
#ifndef _lines_h_
#define _lines_h_

/**
	* This struct is NOT opaque because in some cases caller must control
	* the scan (e.g. when interleaving scans of two files), so callbacks
	* are not appropriate.
	*/
typedef struct _line_stream {

	const char *name;
	FILE *fp;

	/**
		* File location
		*/
	unsigned int count;

	/**
		* Line input buffer state.
		*/
	char   *line;
	size_t  blen;
	ssize_t llen;

	bool file_data_owned;

} line_stream;

extern int  lines_init( line_stream *, const char *name, FILE * );
extern bool lines_next( line_stream * );
extern void lines_free( line_stream * );

#define END_OF_LINES(s) ((s)->llen<1)

#endif

