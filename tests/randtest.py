# vim: ts=2:sw=2:sts=0:noet:nu

from sys import argv
from random import random as unitrand,randint,sample as draw,shuffle
from subprocess import Popen,PIPE,DEVNULL
from os import environ

P_NOISE_LINES = float( environ.get("P_NOISE_LINES","0") )
N_NOISE_LINES =   int( environ.get("N_NOISE_LINES","1") )

JOI = '../joi'
N = int(argv[1]) if len(argv) > 1 else 10

sample = list(range(N));shuffle(sample)

# partition the sample into 4 subsets which will be the left-unique,
# matching, right-unique and unused subsets.
breaks = sorted(draw(range(N),3))
group = [
	sample[         :breaks[0]],
	sample[breaks[0]:breaks[1]],
	sample[breaks[1]:breaks[2]] ]
shuffle( group )

group[0] = [ ('<',n) for n in group[0] ]
group[1] = [ ('=',n) for n in group[1] ]
group[2] = [ ('>',n) for n in group[2] ]

aggregate = sorted(group[0]+group[1]+group[2],key=lambda item:item[1])
stats = "#{}\t{}\t{}".format( len(group[0]), len(group[1]), len(group[2]) )

with open(".ans","w") as fp:
	for pair in aggregate:
		print( *pair, sep="\t", file=fp)
	print( stats, file=fp)


def insert_noise( fp ):
	global P_NOISE_LINES, N_NOISE_LINES
	if unitrand() < P_NOISE_LINES:
		n = draw(range(N_NOISE_LINES),1).pop() + 1
		for _ in range(n):
			print( "# foo" if unitrand() < 0.5 else "", file=fp )


with open(".left","w") as fp:
	for pair in sorted( group[0]+group[1], key=lambda item:item[1] ):
		print( pair[1], file=fp )
		insert_noise( fp )

with open(".right","w") as fp:
	for pair in sorted( group[1]+group[2], key=lambda item:item[1] ):
		print( pair[1], file=fp )
		insert_noise( fp )

joi = Popen([ JOI, "-a", "-s", "#", ".left", ".right" ], stdout=PIPE, stderr=DEVNULL, encoding="ASCII" ).stdout.read()
with open(".ans") as ans_fp:
	ans = ans_fp.read()

if joi != ans:
	print( ans )
	print( joi )
else:
	print( "OK", stats[1:] )

